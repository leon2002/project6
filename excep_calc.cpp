#include <iostream>
#define INVALID_VAL 8200

int add(int a, int b) {
	if (a == INVALID_VAL || b == INVALID_VAL || (a + b) == INVALID_VAL) {
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
	}
	
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for(int i = 0; i < b; i++) {
		if (a == INVALID_VAL || b == INVALID_VAL || sum == INVALID_VAL) {
			throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
		}
		try {
			sum = add(sum, a);
		}
		catch(std::string s){
			std::cout << s << std::endl;
		}
	} // was ;
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for(int i = 0; i < b; i++) {
		try {
			exponent = multiply(exponent, a);
		}
		catch (std::string s) {
			std::cout << s << std::endl;
		}
	} // was ;
	return exponent;
}


int main(void) {


	std::cout << pow(5, 5) << std::endl;
	std::cout << pow(8200, 5) << std::endl;
	try {
		std::cout << add(4000, 4200) << std::endl;
	}
	catch (std::string s) {
		std::cout << s << std::endl;
	}




}
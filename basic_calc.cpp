#include <iostream>
#define INVALID_VAL 8200

bool add(int a, int b, int* result){
	if (a == INVALID_VAL || b == INVALID_VAL || (a + b) == INVALID_VAL) {
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
		return false;
	}
	else {
		*result = a + b;
		return true;
	}
}

bool  multiply(int a, int b, int* result) {
	int sum = 0;
	for(int i = 0; i < b; i++) {
		if (add(a, b, result)) {
			sum += (*result - b);
		}
		if (a == INVALID_VAL || b == INVALID_VAL || sum == INVALID_VAL) {
			std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
			return false;
		}
		else {
			*result = sum;
		}
	} // was ;
	return true;
}

bool  pow(int a, int b, int* result) {
	int exponent = 1;
	for(int i = 0; i < b; i++) {
		if (multiply(a, b, result)) {
			exponent *= (*result/b);
		}
		if (a == INVALID_VAL || b == INVALID_VAL || exponent == INVALID_VAL) {
			std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
			return false;
		}
		else {
			*result = exponent;
		}
	} // was ;
	return true;
}

int main(void) {
	int ans = 0;
	int& result = ans;
	std::cout << pow(5, 5, &result) << std::endl;
	std::cout << *(&result) << std::endl;

	int& result = ans;
	std::cout << multiply(14, 5, &result) << std::endl;
	std::cout << *(&result) << std::endl;

	int& result = ans;
	std::cout << add(124, 6, &result) << std::endl;
	std::cout << *(&result) << std::endl;



}